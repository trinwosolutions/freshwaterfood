<?php

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
require_once __DIR__ . '/vendor/autoload.php';


//Create an instance; passing true enables exceptions
if (!empty($_POST['name']) && !empty($_POST['email'])) {

    $email = $_POST['email'];
    $name = $_POST['name'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $product = $_POST['product'];
    $packing = $_POST['packing'];

    $send_email_to = 'admin@freshwaterfood.ca';

    $mail = new PHPMailer();

    //Server settings
    $mail->isSMTP();                              //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';       //Set the SMTP server to send through
    $mail->SMTPAuth   = true;             //Enable SMTP authentication
    $mail->Username   = 'freshwaterfood1@gmail.com';   //SMTP write your email
    $mail->Password   = 'meaksjezwohhjbkh';      //SMTP password
    $mail->SMTPSecure = 'ssl';            //Enable implicit SSL encryption
    $mail->Port       = 465;

    //Recipients
    $mail->setFrom($email, $name); // Sender Email and name
    $mail->addAddress($send_email_to, 'Admin');     //Add a recipient email
    $mail->addReplyTo($email, $name); // reply to sender email

    //Content
    $mail->isHTML(true);               //Set email format to HTML
    $mail->Subject = 'Enquiry about Fresh Water Foods';   // email subject headings

    $bodyParagraphs = ["Name: {$name}", "Email: {$email}", "Address:", nl2br($address), "Phone:", $phone, "Product:", $product, "Packing:", $packing ];
    $body = join('<br />', $bodyParagraphs);

    $mail->Body    = $body;

    if($mail->send()){
        // Success sent message alert
        echo
        " 
        <script> 
         alert('Enquiry submitted successfully!');
         document.location.href = 'index.html';
        </script>
        ";
    } else {
        // Error alert
        echo
        " 
        <script> 
         alert('Enquiry submission failed. Please try after sometime.');
        </script>
        ";
    }
}

?>
